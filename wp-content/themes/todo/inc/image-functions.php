<?php
/**
 * Create inline styles that enable image focal points created by Smartcrop plugin
 *
 * @package WordPress
 * @subpackage Todo Theme
 * @since 1.0.0
 */


/**
 * Use focus settings on images to write custom css.
 * 
 * @see https://www.wpsmartcrop.com/ 
 * @see  https://developer.wordpress.org/reference/functions/wp_add_inline_style/ 
 */
function todo_smartcrop_styles() {
    global $wpdb;
    $query = "SELECT * FROM wp_postmeta where meta_key='_wpsmartcrop_image_focus'";
    $rows = $wpdb->get_results( $query );
    if (!empty($rows)) {
        $style = '';
        foreach ($rows as $i => $row) {
            $id = $row->post_id;
            $focus = unserialize($row->meta_value);
            $top = $focus['top'];
            $left = $focus['left'];
            $style .= "img.wp-image-$id { object-position: $left% $top%; }\n";
        }
        wp_add_inline_style( 'todo-style', $style );
    }   
}
add_action( 'wp_enqueue_scripts', 'todo_smartcrop_styles' );


/**
 * Make custom image sizes available to editors
 * @param  array $sizes 
 * @return array
 */
function todo_custom_image_size_names ( $sizes ) {
    $custom = todo_get_image_sizes();
    foreach ($custom as $key => $size) {
        $sizes[$key] = ucwords( str_replace('_', ' ', $key) );
    }
    asort($sizes);

    return $sizes;
}

add_filter( 'image_size_names_choose', 'todo_custom_image_size_names' );
