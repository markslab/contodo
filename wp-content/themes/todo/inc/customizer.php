<?php
/**
 * Varya Theme: Customizer
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function todo_customize_register( $wp_customize ) {
	$wp_customize->remove_setting( 'blogname' );
	$wp_customize->remove_setting( 'blogdescription' );
	$wp_customize->remove_control( 'blogname' );
	$wp_customize->remove_control( 'blogdescription' );

	//  Contodo Info SECTION
	$wp_customize->add_section( 'contodo_info', array(
	  'title' => __( 'Contodo Info' ),
	  'description' => __( 'Contact info' ),
	  'priority' => 160,
	  'capability' => 'edit_theme_options',
	) );
	//  Contodo Info OPTIONS
	//		- address
	$wp_customize->add_setting( 'contodo_address', array(
	  'type' => 'option',
	  'capability' => 'manage_options',
	  'default' => "2611 Eighth Street\rBerkeley, CA 94710",
	  'transport' => 'refresh', // or postMessage
	) );
	$wp_customize->add_control( 'contodo_address', array(
	  'type' => 'textarea',
	  'priority' => 10, // Within the section.
	  'section' => 'contodo_info', // Required, core or custom.
	  'label' => __( 'Address' ),
	  'description' => __( 'This is the address that will appear in the contact popover.' ),
	  'input_attrs' => array(
	    'class' => 'my-custom-class-for-js',
	    'placeholder' => __( "123 Fake Street\rSpringfield USA 95555" ),
	  ),
	) );
	//		- email
	$wp_customize->add_setting( 'contodo_email', array(
	  'type' => 'option',
	  'capability' => 'manage_options',
	  'default' => "hola@contodo.co",
	  'transport' => 'refresh', // or postMessage
	) );
	$wp_customize->add_control( 'contodo_email', array(
	  'type' => 'email',
	  'priority' => 10, // Within the section.
	  'section' => 'contodo_info', // Required, core or custom.
	  'label' => __( 'Email' ),
	  'description' => __( 'This is the address that will appear throughout the site.' ),
	  'input_attrs' => array(
	    'placeholder' => __( "hola@contodo.co" ),
	  ),
	) );
	//		- telephone
	$wp_customize->add_setting( 'contodo_telephone', array(
	  'type' => 'option',
	  'capability' => 'manage_options',
	  'default' => "+1 510 393 2305",
	  'transport' => 'refresh', // or postMessage
	) );
	$wp_customize->add_control( 'contodo_telephone', array(
	  'type' => 'text',
	  'priority' => 10, // Within the section.
	  'section' => 'contodo_info', // Required, core or custom.
	  'label' => __( 'Telephone' ),
	  'description' => __( 'This is the phone number that will appear in the contact popover.' ),
	  'input_attrs' => array(
	    'placeholder' => __( "+1 510 393 2305" ),
	  ),
	) );

}
add_action( 'customize_register', 'todo_customize_register', 11 );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function todo_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function todo_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Print custom options in template files.
 *
 * @param      String  $name           The name of the custom option
 * @param      String  $extraCallback  Callback name for print variants (eg. tel without spaces for link)
 */
function todo_customize_option(String $name, String $extraCallback = '') {
	if (!empty($name)) $opt = get_option($name);
	if ($opt) {
		$output = '';
		switch ($name) {
			case 'contodo_address':
				$output = nl2br($opt);
				break;

			default :
				$output = $opt;
				break;

		}
		//  
		if (!empty($extraCallback) && function_exists($extraCallback)) $output = $extraCallback($output);

		print $output;
	}
}
