<?php
/**
 * Custom comment walker for this theme
 *
 * 		Prints div instead of li.  WTF wordpress - this should be easier!
 *
 * @package WordPress
 * @subpackage Todo
 * @since 1.0.0
 */

class Todo_Walker_Nav_Menu extends Walker_Nav_Menu {
  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
  	parent::start_el($output, $item, $depth, $args, $id);
  	$output = preg_replace('/<li /', '<div ', $output);
  }

  public function end_el( &$output, $item, $depth = 0, $args = null ) {
		$output .= "</div>\n";
	}
}
