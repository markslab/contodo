<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */
$url = sprintf( esc_url( get_permalink() ) );
?>
<a href="<?php print($url); ?>" class="text-white">
	<project id="project-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php the_post_thumbnail( 'post-thumbnail' ); ?>
		<div class="filter-layer"></div>
		<div class="entry-content">
			<h2 class="container text-light"><?php print get_the_excerpt(); ?></h2>
			<div class="container"><?php the_title( sprintf( 'See ', esc_url( get_permalink() ) ) ); ?></div>
		</div>
		<!-- .entry-content -->

	</project><!-- #post-${ID} -->
</a>
