<?php
/**
 * Displays social links
 *
 * @package WordPress
 * @subpackage Todo
 * @since 1.0.0
 */

require_once( get_template_directory() . '/classes/class-todo-walker-nav-menu.php' );

?>
<?php
$menu = wp_nav_menu(
	array(
		'menu'           => 'social-links',
		'menu_class'     => '',
		'container'      => false,
		'items_wrap'     => '%3$s',
		'depth'          => 1,
		'walker' => new Todo_Walker_Nav_Menu()
	)
);
?>
