<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Todo
 * @since 1.0.0
 */

require_once( get_template_directory() . '/classes/class-todo-walker-nav-menu.php' );

?>
<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
	
	<nav id="site-navigation" class="main-navigation jumbotron rounded-0 bg-dark position-fixed w-100" aria-label="<?php esc_attr_e( 'Main Navigation', 'todo' ); ?>">
		<div class="container p-0 w-100 mh-100 h-100 m-auto display-block position-relative">
			<div class="row top">
				<div class="col">
					<div class="site-logo">
						<?php if ( !is_front_page() ) : ?><a href="/"><?php endif; ?>
							<img src="/wp-content/themes/todo/images/logo-white.svg" title="Contodo Design Studio Logo" alt="Contodo Logo" \>
						<?php if ( !is_front_page() ) : ?></a><?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row mid">
				<div class="col h-100 w-100 vcenter container">
					<div class="row">
						
					
						<div class="col-md-6 d-none d-md-inline text-light border-right border-light">
							<h2 class="text-white slogan">Creativity,<br /> with Everything.</h2>
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) :
							?>
							<div class="row">
								<div class="col col-lg-10 col-xl-9">
									<p class="site-description text-light"><?php echo $description; ?></p>
								</div>
							</div>
							
							<?php endif; ?>
						</div>
						<div class="col-md-6 container-inline">
							<div class="menu-site-navigation-container row justify-content-around text-center">
								<?php
								$menu = wp_nav_menu(
									array(
										'menu'           => 'site-navigation',
										'theme_location' => 'header',
										'menu_class'     => 'menu-links row justify-content-around text-center',
										'container'      => false,
										'items_wrap'     => '%3$s',
										'depth'          => 1,
										'walker' => new Todo_Walker_Nav_Menu()
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center justify-content-md-between bottom">
				<div class="social col-md text-center text-md-right vbottom order-md-3">
					<?php 
						todo_get_partial( 'template-parts/blocks/social-links'); 
					?>
				</div>
				<div class="mailto col-md text-center text-md-left vbottom order-md-1">
					<div><a class="" href="mailto:<?php todo_customize_option('contodo_email'); ?>"><?php todo_customize_option('contodo_email'); ?></a></div>
				</div>
				<div class="copyright col-md text-center vbottom order-md-2">
					&copy;<?php print date("Y"); ?> Con Todo
				</div>
				
			</div>
		</div>

			
	</nav><!-- #site-navigation -->
<?php endif; ?>