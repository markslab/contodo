<?php
/**
 * Displays the footer nav
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

?>
<div class="footer-navigation">
	<div class="internal-links alignfull">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col px-md-0">
					<?php if ( has_nav_menu( 'footer' ) ) : ?>
					<nav aria-label="<?php esc_attr_e( 'Footer Menu', 'todo' ); ?>">
						<?php
						wp_nav_menu(
							array(
								'menu'           => 'site-navigation',
								'theme_location' => 'footer',
								'menu_class'     => 'footer-menu',
								'depth'          => 1,
							)
						);
						?>
					</nav><!-- .footer-navigation -->
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="external-links alignfull bg-dark">
		<div class="container ">
			
			<div class="row justify-content-end py-4 d-md-none">
				<div class="social col-12 text-right">
					<?php 
						todo_get_partial( 'template-parts/blocks/social-links'); 
					?>
				</div>
				<div class="col-12 text-right">
					<a class="text-white mailto" href="mailto:<?php todo_customize_option('contodo_email'); ?>"><?php todo_customize_option('contodo_email'); ?></a>
				</div>
			</div>

			<div class="row justify-content-end d-none d-md-flex py-2">
				<div class="col-12 text-right social">
					<div class=""><a class="mailto" href="mailto:<?php todo_customize_option('contodo_email'); ?>"><?php todo_customize_option('contodo_email'); ?></a></div>
					<?php 
						todo_get_partial( 'template-parts/blocks/social-links'); 
					?>
				</div>
			</div>
		</div>
	</div>
</div>
