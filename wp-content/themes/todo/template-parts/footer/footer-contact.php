<?php
/**
 * Displays the footer callouts
 *
 * @package WordPress
 * @subpackage Todo
 * @since 1.0.0
 */

?>
<div id="site-contact" class="position-fixed w-100 pb-5 pb-lg-3">
	<div class="container">
		<div class="row">
			<div class="col text-right pt-2">
				<button type="button text-dark" class="close" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="callout col-xl-4 text-center text-xl-left">
				<h2>Let’s talk.</h2>
			</div>
			<div class="col-lg-12 col-xl-8 pt-lg-4">
				<div class="d-md-flex justify-content-between text-center text-md-left">
					<div class="callout">
						<h5 class="primary">Tell us everything.</h5>
						<span><a href="mailto:<?php todo_customize_option('contodo_email'); ?>"><?php todo_customize_option('contodo_email'); ?></a><br />
							<a href="tel:<?php todo_customize_option('contodo_telephone'); ?>"><?php todo_customize_option('contodo_telephone'); ?></a></span>
					</div>
					<div class="callout text-center text-md-left">
						<h5 class="primary">Send us a letter.</h5>
						<span><?php todo_customize_option('contodo_address'); ?></span>
					</div>
					<div class="callout text-center text-md-left">
						<h5 class="primary">Connect with us.</h5>
						<!-- social -->
						<?php 
							todo_get_partial( 'template-parts/blocks/social-links'); 
						?>
						<!-- end social -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>