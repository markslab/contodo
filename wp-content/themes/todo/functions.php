<?php
/**
 * todo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

/**
 * todo only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

define( 'TODO_DIR', "/wp-content/themes/todo" );

add_filter( 'wp_lazy_loading_enabled', '__return_false' );

/**
 * Load a template and pass some vars
 * 
 * @param  String  Template name
 * @param  Array
 */
function todo_get_partial($template_name, $data = []) {
    $template = locate_template($template_name . '.php', false);

    if (!$template) return;

    if ($data) extract($data);

    include($template);
}


if ( ! function_exists( 'todo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function todo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on todo, use a find and replace
		 * to change 'todo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'todo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary Navigation', 'todo' ),
				'footer' => __( 'Footer Navigation', 'todo' ),
				'social' => __( 'Social Links Navigation', 'todo' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Do not enqueue editor styles here.
		// Editor styles are loaded in `enqueue_block_editor_assets`
		// See: todo_editor_theme_variables();
		// add_editor_style( 'style-editor.css' );
		
		// Add custom image sizes 
		$sizes = todo_get_image_sizes();
		foreach ($sizes as $size) call_user_func_array('add_image_size', $size);

		// Update image defaults
		$sizes = todo_get_image_sizes(true);
		foreach ($sizes as $size) {
			update_option( $size[0]."_size_w", $size[1] );
			update_option( $size[0]."_size_h", $size[2] );
			update_option( $size[0]."_size_crop", $size[3] );
		}

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'todo' ),
					'shortName' => __( 'S', 'todo' ),
					'size'      => 16,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'todo' ),
					'shortName' => __( 'M', 'todo' ),
					'size'      => 18,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'todo' ),
					'shortName' => __( 'L', 'todo' ),
					'size'      => 32,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'todo' ),
					'shortName' => __( 'XL', 'todo' ),
					'size'      => 48,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'todo' ),
					'slug'  => 'primary',
					'color' => '#0000FF',
				),
				array(
					'name'  => __( 'Secondary', 'todo' ),
					'slug'  => 'secondary',
					'color' => '#FF0000',
				),
				array(
					'name'  => __( 'Dark Gray', 'todo' ),
					'slug'  => 'foreground',
					'color' => '#444444',
				),
				array(
					'name'  => __( 'Light Gray', 'todo' ),
					'slug'  => 'foreground-light',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'todo' ),
					'slug'  => 'background',
					'color' => '#FFFFFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'todo_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function todo_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'todo' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'todo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'todo_widgets_init' );

/**
 *   Return custom image sizes
 * @param  string  $default_ratio     Which aspect ratio to set up as default
 * @param  boolean $only_default_sizes return only defaults
 * @return array                       Array of argsintended for add_image_size
 *
 * @see https://developer.wordpress.org/reference/functions/add_image_size/  WP documentation for add_image_size function
 * @see https://www.smashingmagazine.com/2016/09/responsive-images-in-wordpress-with-art-direction/  Great article with details about image pipeline in WP
 */
function todo_get_image_sizes($only_default_sizes=false, $default_ratio='1x1') {
	$default_sizes = array(
		'medium' => 782,
		'large' => 1024,
	);
	$sizes = array(
		'extra_extra_large' => 1440,
		'extra_large' => 1280,
		'small' => 640,
		'extra_small' => 540,
	);
	$aspect_ratios = array(
		'1x1',
		'4x3',
		'5x3',
		'16x9',
	);

	if ($only_default_sizes) {
		list($w,$h) = explode('x', $default_ratio);
		foreach ($default_sizes as $size_name => $size) {
			$output[$size_name] = [$size_name, $size, $size/($w/$h), 1];
		}
		return $output;
	}

	$output = array();
	foreach ($aspect_ratios as $ratio) {
		list($w,$h) = explode('x', $ratio);
		$all_sizes = ($ratio == $default_ratio) ? $sizes : $sizes+$default_sizes;
		foreach ($all_sizes as $key => $size) {
			$size_name = ($ratio == $default_ratio) ? $key : $key.'_'.$ratio;
			$output[$size_name] = [$size_name, $size, $size*($h/$w), 1];
		}
	}
	return $output;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function todo_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'todo_content_width', 750 );
}
add_action( 'after_setup_theme', 'todo_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function todo_scripts() {

	// Theme styles
	wp_enqueue_style( 'todo-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );

	// RTL styles
	wp_style_add_data( 'todo-style', 'rtl', 'replace' );

	// Print styles
	wp_enqueue_style( 'todo-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

	// Threaded comment reply styles
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// JQ Bootstrap and them scripts
	wp_enqueue_script('todo-lib-scripts', get_template_directory_uri() . '/js/todo-lib.min.js', array(), wp_get_theme()->get( 'Version' ));
	wp_enqueue_script('todo-theme-scripts', get_template_directory_uri() . '/js/todo.min.js', array(), wp_get_theme()->get( 'Version' ));

	// Homepage
	if ( is_front_page() ) {
		//  Intro animation
		wp_enqueue_script('todo-site-intro', get_template_directory_uri() . '/js/intro.js', array(), wp_get_theme()->get( 'Version' ));
	}

	// Projects
	if ( get_the_ID() === 28  ) {
		//  Intro animation
		wp_enqueue_script('todo-project-anim', get_template_directory_uri() . '/js/project-anim.js', array(), wp_get_theme()->get( 'Version' ));
	}

}
add_action( 'wp_enqueue_scripts', 'todo_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function todo_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'todo_skip_link_focus_fix' );

/**
 * Enqueue theme styles for the block editor.
 */
function todo_editor_theme_variables() {

	// Load the theme styles within Gutenberg.
	wp_enqueue_style( 'todo-editor-variables', get_template_directory_uri() . '/variables-editor.css', false, wp_get_theme()->get( 'Version' ), 'all' );

	// Load the child theme styles within Gutenberg.
	wp_enqueue_style( 'todo-editor-styles', get_template_directory_uri() . '/style-editor.css', false, wp_get_theme()->get( 'Version' ), 'all' );
}
add_action( 'enqueue_block_editor_assets', 'todo_editor_theme_variables' );

/**
 * Smartcrop dynamic css.  Expanded srcset.
 */
require get_template_directory() . '/inc/image-functions.php';

/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-todo-svg-icons.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Print a variable to the debug log.
 * 
 * @param              Any ol type o php variable
 */
function todo_debug ( $var ) {
	file_put_contents(WP_CONTENT_DIR . '/debug.log', var_export($var, true) . "\n", FILE_APPEND);
}
