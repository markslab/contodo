<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'single' );

				if ( is_singular( 'attachment' ) ) {
					// Parent post navigation.
					the_post_navigation(
						array(
							/* translators: %s: parent post link */
							'prev_text' => sprintf( __( '<span class="meta-nav">Published in</span><span class="post-title">%s</span>', 'todo' ), '%title' ),
						)
					);
				} elseif ( is_singular( 'post' ) ) {
					// Previous/next post navigation.
					$next = c2c_get_previous_or_loop_post(true);
					$prev = c2c_get_next_or_loop_post(true);
					if (!empty($next)) $next_text = c2c_get_previous_or_loop_post_link(
						'%link',
						'
							<div class="nav-link">
								<div class="text text-lg-right mr-lg-2 mr-xl-4">
									<span class="meta-nav" aria-hidden="true">' . __( 'Next Project >', 'todo' ) . '</span>
									<span class="screen-reader-text">' . __( 'Next post:', 'todo' ) . '</span> 
									<div class="post-title py-xl-2">%title</div>
									<span class="post-category">Branding & Website</span>
								</div>
								<div class="image mt-4 pt-2 pt-lg-0 mt-lg-0 text-lg-right">
									' . get_the_post_thumbnail($next->ID, 'medium') . '
								</div>
							</div>',
							true
						);
					if (!empty($prev)) $prev_text = c2c_get_next_or_loop_post_link(
						'%link',
						'
							<div class="nav-link">
								<div class="image">
									' . get_the_post_thumbnail($prev->ID, 'medium') . '
								</div>
								<div class="text ml-lg-2 ml-xl-4">
									<span class="meta-nav" aria-hidden="true">' . __( '< Previous Project', 'todo' ) . '</span>
									<span class="screen-reader-text">' . __( 'Previous post:', 'todo' ) . '</span>
									<div class="post-title py-xl-2">%title</div>
									<span class="post-category">Branding & Website</span>
								</div>
							</div>',
							true
						);

					?>
					<nav class="navigation post-navigation" role="navigation" aria-label="Projects">
		        <h2 class="screen-reader-text">Project navigation</h2>
		        <div class="nav-links">
		        	<div class="nav-previous d-none d-lg-inline-block"><?php print $prev_text; ?></div>
		        	<div class="nav-next"><?php print $next_text; ?></div>
		        </div>
			    </nav>
					<?php
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
