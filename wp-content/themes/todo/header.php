<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div id="menu-toggle" class="position-fixed">

	<a href="#menu" class="main-nav-toggle">
		  <div>
		    <div>
		        <span></span>
		        <span></span>
		    </div>
		    <svg>
		        <use xlink:href="#path">
		    </svg>
		    <svg>
		        <use xlink:href="#path">
		    </svg>
		  </div>
	</a>

	<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
	    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36" id="path">
	        <path d="M18,18 L1,18 C1,9 9,1 18,1 C27,1 35,9 35,18"></path>
	    </symbol>
	</svg>

</div>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'todo' ); ?></a>

		<?php if ( !is_home() ) : ?>
			<header id="masthead" class="site-header default-max-width container">
				<div class="site-branding">
					<div class="site-logo">
						<a href="/">
							<img src="/wp-content/themes/todo/images/logo.svg" title="Contodo Design Studio Logo" alt="Contodo Logo" \>
						</a>
					</div>
					<h1 class="site-title screen-reader-text"><?php bloginfo( 'name' ); ?></h1>
				</div><!-- .site-branding -->
			</header><!-- #masthead -->
		<?php endif; ?>
		<?php if ( is_home() ) : ?>
			<header id="masthead" class="site-header default-max-width container">
				<div class="site-branding">
					<div class="site-logo">
						<img src="/wp-content/themes/todo/images/logo-white.svg" title="Contodo Design Studio Logo" alt="Contodo Logo" \>
					</div>
					<h1 class="site-title screen-reader-text"><?php bloginfo( 'name' ); ?></h1>
				</div><!-- .site-branding -->
			</header><!-- #masthead -->
		<?php endif; ?>


		<?php get_template_part( 'template-parts/header/site', 'menu' ); ?>

	<div id="content" class="site-content">
