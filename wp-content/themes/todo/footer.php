<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">

		<?php get_template_part( 'template-parts/footer/footer', 'contact' ); ?>

		<?php get_template_part( 'template-parts/footer/footer', 'nav' ); ?>
		
		<?php /*  privacy
			if ( function_exists( 'the_privacy_policy_link' ) ) {
				the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
			} */
			?>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
