/*
 *    File:  Todo theme menu and contact behaviors.
 */
(function($){
  $(document).ready( function() {
    
    //  Store some elements for reference in Event Listeners
    const mainNavToggle = document.querySelector('a.main-nav-toggle')
    const siteContact = $('#site-contact')
    //  Map #hash locations to visible elements
    const hashMap = {
      menu: '#site-navigation',
      contact: '#site-contact'
    }

    //  Hide all visible elements
    const removeViz = () => {
      for (const prop in hashMap) {
        $(hashMap[prop]).removeClass('enabled');
      }
      document.removeEventListener('keyup', onEsc, true);
      document.removeEventListener('mouseup', onClickHTML, true);
      $('a.main-nav-toggle').removeClass('open');
    }
    //  Show specified element
    const addViz = (id) => {
      removeViz();
      $(id).addClass('enabled');
      $(id).trigger('focus');
      document.addEventListener('keyup', onEsc, true);
    }

    //  Event listeners
    const onMainNavClick = (e) => {
      e.preventDefault()
      window.history.back()
      mainNavToggle.removeEventListener('click', onMainNavClick, true)
    }
    const onEsc = (e) => {
      var key = e.key || e.keyCode;
      if (key === 'Escape' || key === 'Esc' || key === 27)
        window.history.back();
    }
    const onClickHTML = (e) => {
      if ( !siteContact.is(e.target) && siteContact.has(e.target).length === 0 ) {
        removeViz()
      }
    }
    const onHashChange = (e) => {
      if (e.newURL !== e.oldURL) {
        switch (window.location.hash) {
          case '#menu':
            addViz(hashMap['menu']);
            //  handle 
            $('a.main-nav-toggle').addClass('open')
            mainNavToggle.addEventListener('click', onMainNavClick, true );
            break;
          case '#contact':
            addViz(hashMap['contact']);
            document.addEventListener('mouseup', onClickHTML, true);
            break;
          default:
            removeViz();
        }
      }
    }
    
    //  On page load, handle hash if it exists
    onHashChange({oldURL: '', newURL: window.location.href});
    
    //  Listen for hash changes
    window.addEventListener('hashchange', onHashChange, true);

    
    //  All Close buttons will change hash
    $('button.close').click( () => { 
      window.history.back()
      window.trigger('hashchange');
    });
    
  
  });
})(jQuery);