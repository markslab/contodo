function scrollTo(element, to, duration) {
    var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

    var animateScroll = function(){
        currentTime += increment;
        var val = Math.easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

function delayScrollTo() {
	window.setTimeout( () => {
		var el = document.querySelector('project')
    var top = el.getBoundingClientRect().top
		if (top > 0) {
			scrollTo(document.documentElement, top, 1000)
		}
	}, 9500 )
}

Math.easeInOutQuad = function (t, b, c, d) {
  t /= d/2;
	if (t < 1) return c/2*t*t + b;
	t--;
	return -c/2 * (t*(t-2) - 1) + b;
};

const intro = () => {
	//  reveal intro vid
	$('.container.js-intro.d-none').removeClass('d-none')
  //  update background color on Safari Desktop only
  /* console.log(navigator.userAgent); */
  if (navigator.userAgent.indexOf("Safari") !== -1 && navigator.userAgent.indexOf("Macintosh") !== -1
  && navigator.userAgent.indexOf("Chrome") == -1 ){
    $('#site-intro').removeClass('bg-dark').css({backgroundColor: "rgb(58,58,58)"});
  }
	//  test for play permission
	const testvid = document.querySelector('#site-intro video#test-vid');
	let startPlayPromise = testvid.play();
	if (startPlayPromise !== undefined) {

	  //  we have permission, let's go
	  startPlayPromise.then(() => {
	    const vid = document.querySelector('#site-intro video#intro-vid');
	    const webmSource = document.createElement('source');
  		const mp4Source = document.createElement('source');
  		const w = $(window).width();
  		var src = '/wp-content/themes/todo/files/ConTodo_Intro_Slide_';
  		const delay = 750; //  delay before and after playing

  	    //get rid of test source
  	  	testvid.pause()
  	  	testvid.parentNode.removeChild(testvid)

  	  	//  add listeners
  		//  	- on load start video and listen for finish
  		vid.addEventListener( 'canplaythrough', (event) => {
  			//  - indicate playing
  			$('body').addClass('intro-playing')
  			//  - timed events
  			window.setTimeout( () => {
  				//  - play
  				vid.play()
  				//  - show arrow
  				window.setTimeout( () => { $('body').addClass('intro-arrow-visible') }, 3500 )
  				//  - scroll to first project
  				delayScrollTo()
  			}, delay)
  		})
      vid.addEventListener( 'ended', (event) => {
        $('body').addClass('intro-played')
      })

  		//  choose correct size video
  		if (w <= 640) {
  			src += '640-880-FINAL';
  		} else if (w <= 1080) {
  			src += '1080-612';
  		} else {
  			src += '1200-680';
  		}

  		//  add sources
  		webmSource.setAttribute('src', src + '.webm')
  		webmSource.setAttribute('type', 'video/webm')
  		vid.appendChild(webmSource)
  		mp4Source.setAttribute('src', src + '.mp4')
  		mp4Source.setAttribute('type', 'video/mp4')
  		vid.appendChild(mp4Source)
  		vid.load()

	  //  no permission, hide video elements and show backup gif
	  }).catch(error => {
	    console.log(error)
	    $('video').addClass('d-none')
	    $('#site-intro picture').removeClass('d-none')
	    $('body').addClass('intro-playing').addClass('intro-played')
	    delayScrollTo()
	    return;
	  });
	}


  // listen for scroll and show intro when #primary is visible
  $(window).on('resize scroll', () => {
    var p = $("#primary:in-viewport( 100 )")
    if ( p.length ) $('body').addClass("show-branding")
    else $('body').removeClass("show-branding")
  });

}
document.addEventListener("DOMContentLoaded", intro);
