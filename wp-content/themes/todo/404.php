<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area vcenter text-center">
		<main id="main" class="site-main default-max-width">

			<div class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title display-1"><?php _e( '404', 'todo' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'Lo siento! The page you are looking for can’t be found.', 'todo' ); ?></p>
					<a href="/projects/">Back to Projects</a>
				</div><!-- .page-content -->
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
