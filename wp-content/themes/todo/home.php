<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Varya
 * @since 1.0.0
 */

get_header();
?>
	<div id="site-intro" class="bg-dark position-relative vcenter">
		<div class="container js-intro d-none">
			<div class="row">
				<div class="col text-center">
					<div class="intro-vid-container">	
						<video id="test-vid" class="skip-lazy" preload="auto" muted playsinline disablePictureInPicture> 
							<source src="/wp-content/themes/todo/files/test.mp4" type="video/mp4">
						</video>
						<video id="intro-vid" class="skip-lazy" preload="auto" muted playsinline disablePictureInPicture>
						</video>
						<picture class="d-none">
							<source media="(max-width: 672px) and (orientation: portrait)" srcset="/wp-content/themes/todo/files/ConTodo_Intro_Slide_640-880-FINAL.gif" />
							<source media="(max-width: 1112px)" srcset="/wp-content/themes/todo/files/ConTodo_Intro_Slide_1080-612.gif" />
							<img class="hard-working-img" src="/wp-content/themes/todo/files/ConTodo_Intro_Slide_1200-680.gif" alt="Contodo Creative Studio. 'Creativity with everything.' Con Todo is a full-service creative studio designing and building digital products, brands, and experiences from the ground up."  />

						</picture>
					</div>
				</div>
			</div>
		</div>
	
		<noscript>
			<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="intro-vid-container">	
						<img src="/wp-content/themes/todo/files/ConTodo_Intro_Slide_640-880-FINAL.gif" 
								srcset="/wp-content/themes/todo/files/ConTodo_Intro_Slide_640-880-FINAL.gif 640w,
								/wp-content/themes/todo/files/ConTodo_Intro_Slide_1080-612.gif 1080w,
								/wp-content/themes/todo/files/ConTodo_Intro_Slide_1200-680.gif 1200w" />
					</div>
				</div>
			</div>
		</noscript>

	</div>
	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="projects" id="projects">
				<?php
				query_posts( [
					'category_name' => 'project',
					'posts_per_page' => '5',
					'orderby' => 'menu_order',
    			'order'   => 'ASC'
				] );
				if ( have_posts() ) {

					// Load posts loop.
					while ( have_posts() ) {
						the_post();
						get_template_part( 'template-parts/content/content', 'home' );
					}

				}
				?>
			</div>

		</main><!-- .site-main -->

	</section><!-- .content-area -->



<?php
get_footer(); ?>
