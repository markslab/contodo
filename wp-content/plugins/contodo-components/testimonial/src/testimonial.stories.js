/**
 * External dependencies
 */
import React from 'react';
const path = require( 'path' );

/**
 * Internal dependencies
 */
import '../style.css';
import Testimonial from './testimonial.js';

export default { 
	title: 'Components/Testimonial', 
	component: Testimonial,
	argTypes: {
		background: {
			control: {
				type: 'color',
			}
		},
		text: {
			control: {
				type: 'color',
			}
		},
		children: {
			control: {
				type: 'select',
				options: {
					'Man': 'https://images.pexels.com/photos/3271268/pexels-photo-3271268.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
					'Woman 1': 'https://images.pexels.com/photos/4511305/pexels-photo-4511305.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
					'Woman 2': 'https://images.pexels.com/photos/3621159/pexels-photo-3621159.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
				},
			}
		},
		className: {
			control: { 
				disable: true,
			},
		},
	},
	args: {
	  name: 'Mr. Bob Dobolina',
	  title: 'Sales Executive',
	  quote: [ "\"Nothing takes the taste out of peanut butter quite like unrequited love. I'm going to take this God-given gift of being funny, and I'm going to spread it out like peanut butter on everything I do. If you can't control your peanut butter, you can't expect to control your life.\"" ],
	  background: '#444',
	  text: '#fff',
	  children: 'https://images.pexels.com/photos/4511305/pexels-photo-4511305.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
	  className: '',
	},
}

const Template = (args) => (
	<Testimonial {...args}>
		<figure>
			<img src={ args.children } alt='example image' />
		</figure>
	</Testimonial>
);

export const Default = Template.bind({});

export const ClipCircle = Template.bind({});
ClipCircle.args = {
	className: 'is-style-clip-circle',
}

export const ClipEllipse = Template.bind({});
ClipEllipse.args = {
	className: 'is-style-clip-ellipse',
}

export const ClipBubble = Template.bind({});
ClipBubble.args = {
	className: 'is-style-clip-bubble',
}

Default.decorators = [(Story) => <div style={{ width: '100vw', maxWidth: '100%' }}><Story/></div>]

