import { __ } from '@wordpress/i18n';

import { CollabLinks } from './links'; 

import {
  InnerBlocks,
  InspectorControls,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import {
  Button,
  IconButton,
  PanelBody,
} from '@wordpress/components';

import {
  Fragment,
} from '@wordpress/element';

export const edit = (props) => {
  const {
      className,
      attributes: {
        name,
        title,
        links,
        allowedBlocks,
        TEMPLATE,
      },
      setAttributes,
    } = props;

    const onChangeName = ( value ) => {
      setAttributes( { name: value } );
    };

    const onChangeTitle = ( value ) => {
      setAttributes( { title: value } );
    };

    const onAddLink = ( value ) => {
      const lanks=[...links]
      lanks.push( { title: '', url: '' })
      setAttributes( { links: lanks } );
    };

    const onRemoveLink = ( index ) => {
      const lanks=[...links]
      lanks.splice( index, 1 )
      setAttributes( { links: lanks } );
    };

    const onChangeLink = ( value, index ) => {
      const lanks=[...links]
      Object.assign( lanks[index], value)
      setAttributes( { links: lanks } );
    };


    const linkFields = links.map( (link, index) => {
      return (
          <div key={ index } className="collabo-link">
            <RichText
                tagName="span"
                className="title"
                multiline={ false }
                placeholder={ __( 'Link title', 'contodo-components' ) }
                value={ link.title }
                onChange={ (title) => onChangeLink( { title: title }, index) }
              />
            <URLInputButton
              url={ link.url }
              onChange={ (url) => onChangeLink( { url: url }, index) }
              disableSuggestions={ true }
            />
            <IconButton
              className="remove-link"
              icon="no-alt"
              label="Delete link"
              onClick={ () => onRemoveLink( index ) }
            />
          </div>
      );
    } )

    return (
        <div className={ ["contodo-collaborator", className].join(" ") }>
          <InspectorControls key={ props.clientId }>
            <PanelBody
                title={ __( 'Collaborator Links', 'contodo-components' ) }
                initialOpen={true}
            >
              <p>If you don't see a link, it's because the url hasn't been entered yet.</p>
              { linkFields }
              <Button
                isDefault
                onClick={ onAddLink.bind( this ) }
              >Add Link</Button>
            </PanelBody>
          </InspectorControls>

          <div className={ "image" }>
            <InnerBlocks
              allowedBlocks={ allowedBlocks }
              template={ TEMPLATE }
              templateLock={ 'all' } />
          </div>
          <div className={ "content" }>
            <RichText
              tagName="h4"
              multiline={ false }
              placeholder={ __( 'Name…', 'contodo-components' ) }
              value={ name }
              onChange={ onChangeName }
            />
            <RichText
              tagName="h5"
              placeholder={ __( 'Title…', 'contodo-components' ) }
              value={ title }
              onChange={ onChangeTitle }
            />
            <CollabLinks links={ links } />
            <Button
              onClick={ onAddLink.bind( this ) }
            />
          </div>
        </div>
    );
  }
