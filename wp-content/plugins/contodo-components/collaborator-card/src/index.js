import { registerBlockType } from '@wordpress/blocks';

import { __ } from '@wordpress/i18n';

import { edit } from './edit'; 
import { save } from './save'; 


registerBlockType( 'contodo-components/collaborator-card', {
	title: __( 'Collaborator Card', 'contodo-components' ),
	description: __(
		'Collaborator Card',
		'contodo-components'
	),
	category: 'contodo-components',
	icon: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 210 297" height="1122.52" width="793.701"><path d="M19.293 20.348h171.6v167.82h-171.6z" fill="#646464"/><path d="M59.396 133.504c-.707-3.953 60.546-1.582 19.126-60.012C67.287 21.8 101.25 19.08 111.84 22.987c0 0 42.086-11.075 31.466 49.91-10.614 20.585-28.577 41.96 4.936 56.447 43.623 3.766 38.298 37.64 43.805 58.824H19.293c-7.56-2.682 14.9-61.325 40.103-54.665z" stroke="#000" stroke-width=".212"/><g fill="none"><path d="M20.41 216.87l127.756-.756" stroke="#646464" stroke-width="8"/><path d="M21.923 244.083l112.637-1.512M21.923 259.958l110.37-2.268M21.167 275.833l149.68-2.268" stroke="#000"/></g></svg>,
	supports: {
		html: false,
	},
	attributes: {
		name: {
			type: 'array',
			source: 'children',
			selector: 'h4',
		},
		title: {
			type: 'array',
			source: 'children',
			selector: 'h5',
		},
		links: {
			type: 'array',
			default: [ 
				{ title: 'Portfolio', url: '' },
				{ title: 'Instagram', url: '' },
				{ title: 'Linkedin', url: '' },
				{ title: 'Medium', url: '' },
			],
		},
		allowedBlocks:{
			type: 'array',
			default: [ 'core/image' ]
		},
		TEMPLATE: {
			type: 'array',
			default: [
				[ 'core/image', {  } ],
			],
		},
	},
	edit: edit,
	save: save,
} );
