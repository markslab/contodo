import { forwardRef } from '@wordpress/element';

export function CollabLinks ( { links } ) {
  return (
    <div className="collaborator-links">
    { links.map( (link, index) => {
        if ( link.url.length > 0 ) {
          return (
            <a
              key={ index }
              className=""
              href={ link.url }
              target="_blank"
              rel="external noreferrer noopener">
              { link.title }
              <span class="components-visually-hidden">(opens in a new tab)</span>
            </a>
          );
        }
      } )
    }
    </div>
  )
}

export default forwardRef( CollabLinks )
