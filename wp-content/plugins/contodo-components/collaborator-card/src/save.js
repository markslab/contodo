import { __ } from '@wordpress/i18n';

import { CollabLinks } from './links';

import {
  InnerBlocks,
  RichText,
} from '@wordpress/block-editor';

const Name = ( props ) => {
	if (props.value && props.value.length > 0)
		return <RichText.Content tagName="h4" value={ props.value } />
}
const Title = ( props ) => {
	if (props.value && props.value.length > 0)
		return <RichText.Content tagName="h5" value={ props.value } />
}

export const save = ( props ) => {
	const {
		className,
		attributes: {
      name,
      title,
      links,
		},
	} = props;

	return (
		<div className={ className }>
			<div className={ "image" }>
				<InnerBlocks.Content />
			</div>
			<div className={ "content" }>
				<Name value={ name } />
				<Title value={ title } />
				<CollabLinks links={ links } />
			</div>
		</div>
	);
}
