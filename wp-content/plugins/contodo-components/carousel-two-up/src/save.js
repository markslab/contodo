import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  RichText,
  MediaUpload,
} from '@wordpress/block-editor';

const Subtitle = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h5" value={ props.value } />
}
const Title = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h2" value={ props.value } />
}
const Content = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="div" className="description" value={ props.value } />
}
const Link = ( props ) => {
	if (props.url && props.url.length > 0 && props.text && props.text.length > 0) 
		return (
			<a href={ props.url }>{ props.text }</a>
		)
}

export const save = ( props ) => {
	const {
		className,
		attributes: {
			alignment,
      subtitle,
      title,
      linkText,
      url,
      absLink,
      content,
      contentWidth,
      imageWidth,
		},
	} = props;

	return (
		<div className={ ["image-"+imageWidth, "alignfull", "contodo-hero", "two-up", className].join(" ") }>
			<div className={ "image" }>
				<InnerBlocks.Content />
			</div>
			<div className={ "content" }>
				<Subtitle value={ subtitle } />
				<Title value={ title } />
				<Content value={ content } />
				<Link text={ linkText } url={ absLink } />
			</div>
		</div>
	);
}
