<?php

/**
 * Plugin Name: Contodo Components
 * Plugin URI: https://gitlab.com/markslab/contodo
 * Description: This is a plugin with some common components for contodo.co.
 * Version: 1.0
 * Author: mark.is.at.a.computer@gmail.com
 *
 * @package contodo-components
 */

defined( 'ABSPATH' ) || exit;

add_action( 'init', 'contodo_components_init' );

include 'project-card/index.php';
include 'carousel-two-up/index.php';
include 'collaborator-card/index.php';
include 'hero-two-up/index.php';
include 'testimonial/index.php';


function contodo_components_init() {
	add_filter( 'block_categories', function( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'contodo-components',
					'title' => 'Contodo Components',
				),
			)
		);
	}, 5, 2 );
}

/**
 * Register Custom Block Styles
 */
if ( function_exists( 'register_block_style' ) ) {
  function contodo_components_register_block_styles() {
    /**
     * Register stylesheets
     */
    wp_register_style(
      'contodo-list-block-styles',
      plugins_url( '_css/list-style.css', __FILE__ ),
			array(  ),
			filemtime( plugin_dir_path( __FILE__ ) . '_css/list-style.css' )
    );

    /**
     * Register block styles
     */
    register_block_style(
      'core/list',
      array(
        'name'         => 'flex',
        'label'        => 'Flex List',
        'style_handle' => 'contodo-list-block-styles',
      )
    );

    //  Coblocks gallery carousel styles - depends on coblocks plugin
    //if ( function_exists('is_plugin_active') && is_plugin_active('coblocks/class-coblocks.php') ) {
	    wp_register_style(
	      'contodo-coblocks-gallery-carousel-block-arrow-style',
	      plugins_url( '_css/gallery-carousel-arrows-style.css', __FILE__ ),
				array(  ),
				filemtime( plugin_dir_path( __FILE__ ) . '_css/gallery-carousel-arrows-style.css' )
	    );
	    register_block_style(
	      'coblocks/gallery-carousel',
	      array(
	        'name'         => 'hover-arrows',
	        'label'        => 'Slider with arrows',
	        'style_handle' => 'contodo-coblocks-gallery-carousel-block-arrow-style',
	      )
	    );
	  //}


  }

  add_action( 'init', 'contodo_components_register_block_styles' );
}
