import { registerBlockType } from '@wordpress/blocks';

import { __ } from '@wordpress/i18n';

import { edit } from './edit'; 
import { save } from './save'; 


registerBlockType( 'contodo-components/hero-two-up', {
	title: __( 'Hero - Two Up with Image', 'contodo-components' ),
	description: __(
		'Hero - Two Up',
		'contodo-components'
	),
	category: 'contodo-components',
	icon: <svg width="600" height="400" version="1.1" viewBox="0 0 158.75 105.83" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 -191.17)"><rect x="79.914" y="205.86" width="70.826" height="79.379" stroke-width=".26458"/><path d="m14.433 228.85h43.565" fill="none" stroke="#000" stroke-width="4.1372"/><rect x="14.433" y="207.73" width="35.28" height="6.1472" fill="#b3b3b3" stroke-width="3.175"/><rect x="14.433" y="247.02" width="46.238" height="29.132" fill="#b3b3b3" stroke-width="3.175"/><rect x=".26727" y="191.16" width="158.49" height="106.11" fill="none" stroke="#000" stroke-width="3.175"/></g></svg>,
	supports: {
		html: false,
	},
	styles: [
		{
			name: 'darkImageRight',
			label: __('dark BG - image right'),
			isDefault: true
		},
		{
			name: 'darkImageLeft',
			label: __('dark BG - image left')
		},
		{
			name: 'lightImageRight',
			label: __('light BG - image right')
		},
		{
			name: 'lightImageLeft',
			label: __('light BG - image left')
		}
	],
	attributes: {
		subtitle: {
			type: 'array',
			source: 'children',
			selector: 'h5',
		},
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		linkText: {
			type: 'string',
			source: 'text',
			selector: 'a',
		},
		url: {
			type: 'string',
			source: 'attribute',
			selector: 'a',
			attribute: 'href',
		},
		absLink: {
			type: 'string',
		},
		content: {
			type: 'array',
			source: 'children',
			selector: '.description',
		},
		imageWidth: {
			type: 'integer',
			default: 50,
		},
		allowedBlocks:{
			type: 'array',
			default: [ 'core/image' ]
		},
		TEMPLATE: {
			type: 'array',
			default: [
				[ 'core/image', {  } ]
			],
		},
	},
	edit: edit,
	save: save,
} );
