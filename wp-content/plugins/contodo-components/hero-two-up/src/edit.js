import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  InspectorControls,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { 
  Button,
  PanelBody,
  PanelRow,
  SelectControl,
} from '@wordpress/components';

export const edit = (props) => {
  const {
      className,
      attributes: {
        alignment,
        subtitle,
        title,
        linkText,
        url,
        absLink,
        content,
        imageWidth,
        allowedBlocks,
        TEMPLATE,
      },
      setAttributes,
    } = props;

    const getAbsLink = ( url ) => {
      var base_url = window.location.origin
      return url.substring( base_url.length )
    }
    
    const onChangeAlignment = ( value ) => {
      setAttributes( { alignment: value } );
    }

    const onChangeSubtitle = ( value ) => {
      setAttributes( { subtitle: value } );
    };

    const onChangeTitle = ( value ) => {
      setAttributes( { title: value } );
    };

    const onChangeLinkText = ( value ) => {
      setAttributes( { linkText: value } );
    };

    const onChangeURL = ( value ) => {
      setAttributes( { url: value, absLink: getAbsLink(value) } );
    };

    const onChangeContent = ( value ) => {
      setAttributes( { content: value } );
    };

    return (
      <>
        <InspectorControls>
          <PanelBody
              title={ __( 'Image Width', 'contodo-components' ) }
              initialOpen={true}
          >
            <PanelRow>
              <SelectControl
                label="Image Width"
                value={ imageWidth }
                className="d-block w-100 mb-2"
                options={ [
                  { label: '25%', value: 25 },
                  { label: '33%', value: 33 },
                  { label: '42%', value: 42 },
                  { label: '50%', value: 50 },
                  { label: '58%', value: 58 },
                  { label: '67%', value: 67 },
                  { label: '75%', value: 75 },
                ] }
                onChange={ ( imageWidth ) => { setAttributes( { imageWidth: imageWidth } ) } }
                />
            </PanelRow>
          </PanelBody>
        </InspectorControls>
        <div className={ ["alignfull", "contodo-hero", "two-up", "image-"+imageWidth, className].join(" ") }>
          <div className={ "image" }>
            <InnerBlocks 
              allowedBlocks={ allowedBlocks } 
              template={ TEMPLATE } 
              templateLock={ 'all' } />
          </div>
          <div className={ "content" }>
            <RichText
              tagName="h5"
              multiline={ false }
              placeholder={ __( 'Subtitle…', 'contodo-components' ) }
              value={ subtitle }
              onChange={ onChangeSubtitle }
            />
            <RichText
              tagName="h2"
              placeholder={ __( 'Title…', 'contodo-components' ) }
              value={ title }
              onChange={ onChangeTitle }
            />
            <RichText
              tagName="div"
              multiline="p"
              className="description"
              placeholder={ __( 'Content…', 'contodo-components' ) }
              value={ content }
              onChange={ onChangeContent }
            />
            <div className="container-inline">
              <RichText
                tagName="a"
                multiline={ false }
                className="description"
                placeholder={ __( 'Link…', 'contodo-components' ) }
                value={ linkText }
                onChange={ onChangeLinkText }
              />
              <URLInputButton
                className="project-url in-line"
                url={ url }
                placeholder={ __( 'Link…', 'contodo-components' ) }
                onChange={ onChangeURL }
              />
            </div>
          </div>
        </div>
      </>
    );
  }